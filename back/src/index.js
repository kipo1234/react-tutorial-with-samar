/*import { createServer } from 'http'

const whenRequestReceived = (request ,response ) => {
    response.writeHead(200, { 'Content-type': `text/plain` });
response.write('Hello');
response.end();


}
const server = createServer(whenRequestReceived);
server.listen(8080, ()=>{console.log('ok, i am listening')});
*/


// back/src/index.js
import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase()
  const contacts_list = await controller.getContactsList()
  console.log(contacts_list)
  //app.get('/',(req,res)=>res.send("ok"));
  //app.listen(8080, ()=>console.log('server listening on port 8080'))
}

start();


